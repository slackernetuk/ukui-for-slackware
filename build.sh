#!/bin/bash 

for i in \
libuchardet \
geoclue2 \
libgusb \
colord \
lsb-release \
imlib2 \
iniparser \
gnome-common \
bamf \
gsettings-qt \
ukui-kwin \
qt5-ukui-platformtheme \
peony \
ukui-menu \
mate-common \
ukui-menus \
ukui-biometric-auth \
lxqt-build-tools \
libqtxdg \
ukui-panel \
ukui-sidebar \
ukui-interface \
ukui-media \
ukui-power-manager \
gnome-autoar \
gnome-desktop \
ukui-screensaver \
ukui-settings-daemon \
ukui-control-center \
ukui-themes \
ukui-wallpapers \
ukui-session-manager \
ukui-window-switch \
kylin-nm \
; do
cd ${i} || exit 1
./${i}.SlackBuild || exit 1
cd ..
done
